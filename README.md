# Open GDC

Welkom op de Open GDC Gitlab. De GDC staat voor Gemeenschappelijke data catalogus en is een initatief van samenwerkende Nederlandse gemeenten. De GDC is community sourced software. Community source software is open source software die door een actieve community wordt gebruikt, doorontwikkeld en beheerd. 

De GDC wordt op dit moment ingezet als data catalogus door verschillende gemeenten. De GDC wordt daarnaast doorontwikkeld zodat ook andere objecten in het register kunnen worden gepubliceerd zoals diensten, regels en documenten. 

De GDC is in essentie een publicatieplatform voor overheden om actieve en passieve openbaarmaking van data, documenten en informatieproducten te ondersteunen. De GDC biedt ondersteuning voor de uitvoer van de Wet Hergebruik Overheidsinformatie (Who) en de Wet Open Overheid (Woo).

## GDC community

De GDC community is een informeel samenwerkingsverband van overheidsorganisaties. De GDC is geen rechtspersoon. Alleen overheidsorganisaties kunnen op dit moment deelnemen in de community. 

Het is mogelijk om de GDC software voor andere doeleinden in te zetten, of zelf een community voor andere toepassingen of organisaties op te zetten. Laat ons even weten als je hierbij hulp nodig hebt of andere ideëen wilt delen. 

De code van de GDC staat op deze Gitlab en is hier te vinden: https://gitlab.com/gdc-nl/datacatalogus-software

## GDC in het datalandschap van de overheid

De GDC is een openbaar publicatieplatform. Gemeenten kunnen handmatig of via een API de informatie over beschikbare data en documenten invoeren in de catalogus. Databestanden kunnen met een link naar de vindplaats worden opgenomen, of de data kan als bestand op de GDC worden gepubliceerd. 

Vanuit de GDC kan alle open data direct naar data.overheid.nl worden gepubliceerd. Data.overheid.nl publiceert op haar beurt naar het Europese dataportaal. 

De GDC volgt zoveel mogelijk de richtlijnen van Common Ground. Data en documenten worden zo min mogelijk gekopieerd. De GDC verwijst waar mogelijk naar de vindplaats van data en document, en kopieert de data niet. Alleen wanneer er geen publicatie locatie is, wordt data op de GDC zelf gepubliceerd. De GDC is dus feitelijk een telefoonboek met informatie over de data en document en met verwijzingen naar de vindplaats. 

## Ondersteuning

De GDC is ontwikkeld in opdracht van de samenwerkende gemeenten door Dexes. De gemeente Utrecht en Groningen hebben de basis gelegd voor de GDC. Gemeente Nijmegen heeft de initiele software geleverd die op de open source code van data.overheid.nl en de Europese samenwerking is gebaseerd. 

Dexes is een marktpartij die eerder aan de wieg stond van het Nederlandse dataportaal data.overheid.nl. Dexes biedt de GDC software als Saas dienst aan, met enkele aanvullende diensten. Dexes is een startup gevestigd in Utrecht en voert projecten uit rond data op orde, veilig data delen en de ondersteuning actieve openbaarmaking voor lokale en rijksoverheden. Dexes is mede-oprichter van de Amsterdam Data Exchange (AMdEX) en deelnemer in het DMI ecosysteem voor ondersteuning van data uitwisseling in Smart City en mobiliteit samenwerking.  

## GDC deelnemer worden? 

Overheidsorganisaties kunnen deelnemer worden in de GDC community. Deelnemers gebruiken de GDC software, dragen bij aan de ontwikkeling en beheer van de software en de community. Op dit moment zijn de deelnemers: 

- Gemeente Utrecht - https://data.utrecht.nl 
- Gemeente Groningen - https://data.groningen.nl
- Gemeente Nijmegen - https://opendata.nijmegen.nl
- Gemeente Oss - https://data.oss.nl 
- Gemeente Land van Cuijk - https://data.gemeentelandvancuijk.nl/ (i.o.)

Met meerdere andere gemeenten vindt op dit moment (februari 2024) afstemming plaats over deelname aan de GDC community. 

## Notes
Het beheer van deze Gitlab vindt bij roulatie plaats door één van de GDC deelnemers. 
Wil je verwijzen naar de GDC, gebruik dan de link: opengdc.nl. Deze link leidt op dit moment naar deze GitLab pagina. 

